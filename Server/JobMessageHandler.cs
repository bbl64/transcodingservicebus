﻿using System;
using System.IO;
using System.Threading.Tasks;
using NServiceBus;
using NServiceBus.Logging;
using TranscodeProcess.Transcoding;
using TranscodeProcess.InputProcessing;
using TranscodeProcess.InputProcessing.InputStrategy;


#region JobMessageHandler
/// <summary>
/// This class is responsible for handling all Asynchronous Job Messages.
/// </summary>
public class JobMessageHandler :
    IHandleMessages<JobCommand>
{
    static ILog log = LogManager.GetLogger<JobMessageHandler>();

    public Task Handle(JobCommand message, IMessageHandlerContext context)
    {
        log.Info("Starting up the JobMessageHandler");
        if( message.JobFileBlob != null ) log.Info($"Message received, size of blob property: {message.JobFileBlob.Length} Bytes");
        var reply = context.Reply(ErrorCodes.Success);

        // Set a filename
        var timeStamp = GetTimestamp(DateTime.Now);
        var filePath = "jobs_" + timeStamp + ".csv";

        var result = ByteArrayToFile(filePath, message.JobFileBlob);
        if (result) RunTranscoding(filePath);
        else reply = context.Reply(ErrorCodes.Fail);

        return reply;
    }

    /// <summary>
    /// Using the TranscodeProcess.bbl64 NuGet Package to process jobs
    /// </summary>
    /// <param name="fileName"></param>
    private void RunTranscoding(string fileName)
    {
        // Get jobs
        var loadService = new LoadJobsService(new CsvFileStrategy(fileName));
        var jobs = loadService.LoadJobs();

        // Make the analysis
        var transcodeProcess = new TranscoderProcess(jobs);
        transcodeProcess.JobGroupAnalysis();

        // Print Reslt
        transcodeProcess.PrintGroups();
    }

    private bool ByteArrayToFile(string fileName, byte[] byteArray)
    {
        try
        {
            using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                fs.Write(byteArray, 0, byteArray.Length);
                return true;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Exception caught in process: {0}", ex);
            return false;
        }
    }

    private string GetTimestamp(DateTime value)
    {
        return value.ToString("yyyyMMddHHmmssffff");
    }

}
#endregion
