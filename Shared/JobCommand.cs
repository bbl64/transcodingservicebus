﻿using System;
using NServiceBus;

[Serializable]
public class JobCommand :
    ICommand
{
    public byte[] JobFileBlob { get; set; }
}
