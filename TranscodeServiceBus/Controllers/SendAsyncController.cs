﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using NServiceBus;

public class SendAsyncController :
    AsyncController
{
    IEndpointInstance endpoint;

    public SendAsyncController(IEndpointInstance endpoint)
    {
        this.endpoint = endpoint;
    }

    [HttpGet]
    public ActionResult Index()
    {
        ViewBag.Title = "SendAsync";
        return View("Index");
    }

    [HttpPost]
    [AsyncTimeout(50000)]
    public async Task<ActionResult> FileUpload(HttpPostedFileBase file)
    {
        #region AsyncController

        var bytes = CopyToByteArray(file);
        
        var message = new JobCommand
        {
            JobFileBlob = bytes //5MB
        };

        var sendOptions = new SendOptions();
        sendOptions.SetDestination("Mvc.Transcode.Server");

        //var status = await endpoint.Send("Mvc.Transcode.Server", message)
          //  .ConfigureAwait(false);

        var status = await endpoint.Request<ErrorCodes>(message, sendOptions)
            .ConfigureAwait(false);


         return IndexCompleted(Enum.GetName(typeof(ErrorCodes), status));

        #endregion
    }

    private byte[] CopyToByteArray(HttpPostedFileBase file)
    {
        var target = new MemoryStream();
        file.InputStream.CopyTo(target);
        return target.ToArray();
    }

    public ActionResult IndexCompleted(string errorCode)
    {
        ViewBag.Title = "SendAsync";
        ViewBag.ResponseText = errorCode;
        return View("Index");
    }
}